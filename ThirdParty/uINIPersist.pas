{$M+}
unit uINIPersist;

interface

uses
  Classes, SysUtils, Rtti, TypInfo, IniFiles;

type
  /// USAGE
  /// [IniValue('XMLProcessor', 'HOST', '')]
  ///  Property HOST: string

  IniValueAttribute = class(TCustomAttribute)
  private
    FName: string;
    FDefaultValue: string;
    FSection: string;
  published
     constructor Create(const sSection : string; const sName : string; const sDefaultValue : String = '');
     property Section : string read FSection write FSection;
     property Name : string read FName write FName;
     property DefaultValue : string read FDefaultValue write FDefaultValue;
  end;

  TMemIniFile = class(IniFiles.TMemIniFile)
  protected
    procedure LoadValues;
  public
    constructor Create(const FileName: string); overload;
    constructor Create(const FileName: string; const Encoding: TEncoding); overload;
    procedure Rename(const FileName: string; Reload: Boolean);
  end;

  TIniPersist = class (TObject)
  private
    class procedure SetValue(Data: string; var Value: TValue);
    class function GetValue(var Value: TValue): string;
    class function GetIniAttribute(Obj: TRttiObject): IniValueAttribute;
  public
    class procedure Load(FileName: string; Obj: TObject);
    class procedure MemLoad(FileName: string; Obj: TObject; AFileEncoding: TEncoding = nil);
    class procedure Save(FileName: string; Obj: TObject);
  end;

  EIniPersist = class(Exception);

implementation

{ IniValueAttribute }

constructor IniValueAttribute.Create(const sSection, sName,
  sDefaultValue: string);
begin
  FSection      := sSection;
  FName         := sName;
  FDefaultValue := sDefaultValue;
end;

{ TIniPersist }

class function TIniPersist.GetIniAttribute(Obj: TRttiObject): IniValueAttribute;
var
  Attr: TCustomAttribute;
begin
  Result := nil;
  for Attr in Obj.GetAttributes do  begin
    if Attr is IniValueAttribute then begin
      Result := IniValueAttribute(Attr);
      Break;
    end;
  end;
end;

class function TIniPersist.GetValue(var Value: TValue): string;
//var
// i : Integer;
begin
  Result := Value.ToString;
//  case Value.Kind of
//    tkWChar,
//    tkLString,
//    tkWString,
//    tkString,
//    tkChar,
//    tkUString : Result := Value.AsString;
//    tkInteger,
//    tkInt64  : Result := Value.AsString;
//    tkFloat  : Result := Value.AsString;
//    tkEnumeration:  Result := GetEnumName(Value.TypeInfo, Value.AsOrdinal) ;//Value := TValue.FromOrdinal(Value.TypeInfo, GetEnumValue(Value.TypeInfo, Data));
//    tkSet: begin
////            Result := SetToString(Value.TypeInfo, Value);
////             i :=  StringToSet(Value.TypeInfo,Data);
////             TValue.Make(@i, Value.TypeInfo, Value);
//          end;
//    else raise EIniPersist.Create('Type not Supported');
//  end;
end;

class procedure TIniPersist.Load(FileName: string; Obj: TObject);
var
 ctx: TRttiContext;
 objType: TRttiType;
 Field: TRttiField;
 Prop: TRttiProperty;
 Value: TValue;
 IniValue: IniValueAttribute;
 Ini: TIniFile;
 Data: string;
begin
 ctx := TRttiContext.Create;
 try
   Ini := TIniFile.Create(FileName);
   try
     objType := ctx.GetType(Obj.ClassInfo);
     for Prop in objType.GetProperties do begin
       IniValue := GetIniAttribute(Prop);
       if Assigned(IniValue) then begin
          Data := Ini.ReadString(IniValue.Section, IniValue.Name, IniValue.DefaultValue);
          Value := Prop.GetValue(Obj);
          SetValue(Data, Value);
          if Prop.IsWritable then
            Prop.SetValue(Obj, Value);
       end;
     end;
     for Field in objType.GetFields do begin
       IniValue := GetIniAttribute(Field);
       if Assigned(IniValue) then begin
          Data := Ini.ReadString(IniValue.Section, IniValue.Name, IniValue.DefaultValue);
          Value := Field.GetValue(Obj);
          SetValue(Data, Value);
          Field.SetValue(Obj, Value);
       end;
     end;
   finally
     Ini.Free;
   end;
 finally
   ctx.Free;
 end;
end;

// ��� ����������� ����� ��� ������ �� INI �����
class procedure TIniPersist.MemLoad(FileName: string; Obj: TObject; AFileEncoding: TEncoding);
var
 ctx: TRttiContext;
 objType: TRttiType;
 Field: TRttiField;
 Prop: TRttiProperty;
 Value: TValue;
 IniValue: IniValueAttribute;
 Ini: TMemIniFile;
 Data: string;
begin
 ctx := TRttiContext.Create;
 try
   Ini := TMemIniFile.Create(FileName, AFileEncoding);
   try
     objType := ctx.GetType(Obj.ClassInfo);
     for Prop in objType.GetProperties do begin
       IniValue := GetIniAttribute(Prop);
       if Assigned(IniValue) then begin
          Data := Ini.ReadString(IniValue.Section, IniValue.Name, IniValue.DefaultValue);
          Value := Prop.GetValue(Obj);
          SetValue(Data, Value);
          if Prop.IsWritable then
            Prop.SetValue(Obj, Value);
       end;
     end;
     for Field in objType.GetFields do begin
       IniValue := GetIniAttribute(Field);
       if Assigned(IniValue) then begin
          Data := Ini.ReadString(IniValue.Section, IniValue.Name, IniValue.DefaultValue);
          Value := Field.GetValue(Obj);
          SetValue(Data, Value);
          Field.SetValue(Obj, Value);
       end;
     end;
   finally
     Ini.UpdateFile;
     Ini.Free;
   end;
 finally
   ctx.Free;
 end;
end;


class procedure TIniPersist.Save(FileName: string; Obj: TObject);
var
  ctx : TRttiContext;
  objType : TRttiType;
  Field : TRttiField;
  Prop  : TRttiProperty;
  Value : TValue;
  IniValue : IniValueAttribute;
  Ini : TIniFile;
  sData: string;
begin
  ctx := TRttiContext.Create;
  try
    Ini := TIniFile.Create(FileName);
    try
      objType := ctx.GetType(Obj.ClassInfo);
      for Prop in objType.GetProperties do
      begin
        IniValue := GetIniAttribute(Prop);
        if Assigned(IniValue) and (Prop.IsReadable) then
        begin
          Value := Prop.GetValue(Obj);
          sData := GetValue(Value);
          sData:= StringReplace(sData,  #13#10, #01, [rfReplaceAll]);
          Ini.WriteString(IniValue.Section, IniValue.Name, sData);
        end;
      end;
      for Field in objType.GetFields do
      begin
        IniValue := GetIniAttribute(Field);
        if Assigned(IniValue) then
        begin
          Value := Field.GetValue(Obj);
          sData := GetValue(Value);
          Ini.WriteString(IniValue.Section, IniValue.Name, sData);
        end;
      end;
    finally
      Ini.Free;
    end;
  finally
    ctx.Free;
  end;
end;

class procedure TIniPersist.SetValue(Data: string; var Value: TValue);
var
 i : Integer;
begin
 case Value.Kind of
   tkWChar,
   tkLString,
   tkWString,
   tkString,
   tkChar,
   tkUString : Value := Data;
   tkInteger,
   tkInt64  : Value := StrToInt(Data);
   tkFloat  : Value := StrToFloat(Data);
   tkEnumeration:  Value := TValue.FromOrdinal(Value.TypeInfo, GetEnumValue(Value.TypeInfo, Data));
   tkSet: begin
             i := StringToSet(Value.TypeInfo, Data);
             TValue.Make(@i, Value.TypeInfo, Value);
          end;
   else raise EIniPersist.Create('Type not Supported');
 end;
end;
{ TMemIniFile }

constructor TMemIniFile.Create(const FileName: string);
begin
  Create(Filename, nil);
end;

constructor TMemIniFile.Create(const FileName: string;
  const Encoding: TEncoding);
begin
  inherited Create(FileName, Encoding);
  Clear;
  LoadValues;
end;

procedure TMemIniFile.LoadValues;
var
  Size: Integer;
  Buffer: TBytes;
  List: TStringList;
  Stream: TFileStream;
  AEncoding: TEncoding;
  ABOM: Cardinal;
begin
  AEncoding := Encoding;
  if (FileName <> '') and FileExists(FileName) then
  begin
    Stream := TFileStream.Create(FileName, fmOpenRead or fmShareDenyWrite);
    try
      // Detect is UTF8-BOM
      Stream.Read(ABOM, 4);
      if (ABOM and $00BFBBEF) = $00BFBBEF then
        Stream.Position := 3
      else
        Stream.Position := 0;

     // Load file into buffer and detect encoding
      Size := Stream.Size - Stream.Position;
      SetLength(Buffer, Size);
      Stream.Read(Buffer[0], Size);
      Size := TEncoding.GetBufferEncoding(Buffer, AEncoding);

      // Load strings from buffer
      List := TStringList.Create;
      try
        List.Text := AEncoding.GetString(Buffer, Size, Length(Buffer) - Size);
        SetStrings(List);
      finally
        List.Free;
      end;
    finally
      Stream.Free;
    end;
  end
  else
    Clear;
end;

procedure TMemIniFile.Rename(const FileName: string; Reload: Boolean);
begin
  inherited Rename(FileName, Reload);
  if Reload then begin
    Clear;
    LoadValues;
  end;
end;

end.
