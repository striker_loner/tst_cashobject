unit uCashObject;

interface

uses
  Classes, SysUtils, Generics.Collections;

  {
  1. ��� �������� � ������������ ������  TCashObject
  2. ���� ����������� �� ������� ����� ������ ���� ����������������� � ������
  ������������� ������� ��� ������ RegisterCashObject
  }
type
  TCashObjectItem = class(TObject)
  private
    FID: Integer;
    FItemObject: TObject;
    FUseLast: TDateTime;
    FUseCount: Integer;
    procedure SetID(const Value: Integer);
    procedure SetItemObject(const Value: TObject);
    procedure SetUseCount(const Value: Integer);
    procedure SetUseLast(const Value: TDateTime);
  protected
  public
    constructor Create; virtual;
    destructor Destroy; override;

    property ID: Integer read FID write SetID;
    property ItemObject: TObject read FItemObject write SetItemObject;
    property UseLast: TDateTime read FUseLast write SetUseLast;
    property UseCount: Integer read FUseCount write SetUseCount;
  end;

  TCustomCash = class(TObject)
  private
    FCount: Integer;
    FCapacity: Integer;
  protected
    procedure SetCapacity(const Value: Integer); virtual;
    procedure SetCount(const Value: Integer); virtual;
    function GetCapacity: Integer; virtual;
    function GetCount: Integer; virtual;

  public
    constructor Create; virtual;
    destructor Destroy; override;

    function SetObjectItem(ACashObjectItem: TCashObjectItem): TCashObjectItem; overload; virtual; abstract;
    function SetObjectItem(AObjectItem: TObject; AObjectID: Integer = -1): TCashObjectItem; overload; virtual; abstract;
    function GetObjectItem(AObjectID: Integer): TCashObjectItem; virtual; abstract;
    procedure DeleteObjectItem(AObjectID: Integer); virtual; abstract;
    function ExtractObjectItem(AObjectID: Integer): TCashObjectItem; virtual;
    // ���������
    function First: TCashObjectItem; virtual;
    function Next(AIndex: Integer = 1): TCashObjectItem; virtual; abstract;
    procedure Clear; virtual; abstract;
    procedure Flush; virtual; abstract;

    property Count: Integer read GetCount write SetCount;
    property Capacity: Integer read GetCapacity write SetCapacity;
  end;

implementation

{ TCashObject }

constructor TCashObjectItem.Create;
begin
  inherited Create;
  FItemObject := nil;
  FUseLast := Now;
  FUseCount := 0;
end;

destructor TCashObjectItem.Destroy;
begin
  FItemObject.Free;
  inherited;
end;

procedure TCashObjectItem.SetID(const Value: Integer);
begin
  FID := Value;
end;

procedure TCashObjectItem.SetItemObject(const Value: TObject);
begin
  FItemObject := Value;
end;

procedure TCashObjectItem.SetUseCount(const Value: Integer);
begin
  FUseCount := Value;
end;

procedure TCashObjectItem.SetUseLast(const Value: TDateTime);
begin
  FUseLast := Value;
end;

{ TCustomCash }

constructor TCustomCash.Create;
begin
  inherited Create;
  Count := 0;
  Capacity := 0;
end;

destructor TCustomCash.Destroy;
begin

  inherited;
end;

function TCustomCash.ExtractObjectItem(AObjectID: Integer): TCashObjectItem;
begin
  Result := GetObjectItem(AObjectID);
  if Assigned(Result) then
    DeleteObjectItem(AObjectID);
end;

function TCustomCash.First: TCashObjectItem;
begin
  Result := Next(0);
end;

function TCustomCash.GetCapacity: Integer;
begin
  Result := FCapacity;
end;

function TCustomCash.GetCount: Integer;
begin
  Result := FCount;
end;

procedure TCustomCash.SetCapacity(const Value: Integer);
begin
  FCapacity := Value;
end;

procedure TCustomCash.SetCount(const Value: Integer);
begin
  FCount := Value;
end;

end.
