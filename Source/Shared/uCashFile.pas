unit uCashFile;

interface

uses
  Classes, SysUtils, Windows, RTTI, DateUtils, uCashObject, IOUtils;

const
  cnstBuffSize = 4096;

type
  /// <summary>
  /// ����� ��� �������������� �������� � ������ ������
  ///  �����������:
  ///   1. ����������� �������� ������ ���� ��� ����������
  ///   2. ��� ����������� ��������� ��������
  ///   3. ��� �������������� ��������.
  /// </summary>
  TConverterObjectBytes = class
  private
  protected
    class function GetClassId(ASource: TObject): string;
    class function NewInstance(AClassId: string): TObject;

    class function StrToBytes(ASource: string): TBytes;
    class function BytesToStr(ASource: TBytes; var AStr: string; AOffset: Integer): Integer;

    class function SimpleTypeRevert(var AValue: TValue; const AData: TBytes; AOffset: Integer = -1): Integer;
    class function SimpleTypeConvert(AValue: TValue; var AData: TBytes; AOffset: Integer = -1): Integer;
  public
    class function ObjectToBytes(ASource: TObject): TBytes;
    class function BytesToObject(ABytes: TBytes): TObject;
  end;

  /// ������ ����� ��� �������� �������������� ��������
  ///  ObjectID     - 4 int
  ///  Deleted falg - 1 byte
  ///  LastDateTime - 8 int64 (DateTime Unix format)
  ///  LastCount    - 4 int
  ///  ObjectSize   - 4 int
  ///  ObjectData   - (ObjectSize) bytes
  TCashFile = class(TCustomCash)
  private
    FStream: TFileStream;
    FFileName: TFileName;
    FCurrentPos: Int64;
    function HEXStreamReadConverted(AStream: TStream; const ADataConvertedSize: Integer): TBytes;
    function HEXStreamWriteConverted(AStream: TStream; const ADataConverted: TBytes): Integer;

    // ���������� ������� ������ ������ � �����
    function StreamFindObjectPos(AObjectID: Integer): Int64;
    function FindObjectPos(AObjectID: Integer): Int64;
    function FindObjectRead(AObjectID: Integer): TCashObjectItem;
    function FindObjectWrite(ACashObjectItem: TCashObjectItem): Int64;

    procedure SetFileName(const Value: TFileName);
    procedure StreamFree;
  public
    constructor Create; override;
    destructor Destroy; override;

    function SetObjectItem(ACashObjectItem: TCashObjectItem): TCashObjectItem; overload; override;
    function SetObjectItem(AObjectItem: TObject; AObjectID: Integer = -1): TCashObjectItem; overload; override;
    function GetObjectItem(AObjectID: Integer): TCashObjectItem; override;
    procedure DeleteObjectItem(AObjectID: Integer); override;
    procedure Clear; override;
    procedure Flush; override;
    function Next(AIndex: Integer = 1): TCashObjectItem; override;

    property FileName: TFileName read FFileName write SetFileName;
  end;

implementation

{ TCashFile }

{ TCashFile }

procedure TCashFile.Clear;
begin
  FStream.Size := 0;
  Count := 0;
end;

constructor TCashFile.Create;
begin
  inherited;
  FStream := nil;
end;

procedure TCashFile.DeleteObjectItem(AObjectID: Integer);
var
  lObjectId: Integer;
  lIsDelete: Byte;
  lPos: Int64;
  lCount: Integer;
begin
  lPos := FindObjectPos(AObjectID);
  if (lPos >= 0) and (lPos < FStream.Size) then begin
    FStream.Position := lPos + SizeOf(lObjectId);
    lIsDelete := 1;
    lCount := FStream.Write(lIsDelete, SizeOf(lIsDelete));
    Count := Count - 1;
  end;
end;

destructor TCashFile.Destroy;
begin
  StreamFree;
  inherited;
end;

function TCashFile.FindObjectRead(AObjectID: Integer): TCashObjectItem;
var
  lObjectId: Integer;
  lIsDelete: Byte;
  lCount: Integer;
  lLastDatetime: Int64;
  lLastCount: Integer;
  lSize: Integer;
  lData: TBytes;
  lOBJ: TObject;
  lPos: Int64;
begin
  Result := nil;
  lPos := FindObjectPos(AObjectID);
  if (lPos >= 0) and (lPos < FStream.Size) then begin
    FStream.Position := FCurrentPos;
    lCount := FStream.Read(lObjectId, SizeOf(lObjectId));
    lCount := FStream.Read(lIsDelete, SizeOf(lIsDelete));
    lCount := FStream.Read(lLastDatetime, SizeOf(lLastDatetime));
    lCount := FStream.Read(lLastCount, SizeOf(lLastCount));
    lCount := FStream.Read(lSize, SizeOf(lSize));
    SetLength(lData, lSize);
    lCount := FStream.Read(lData, lSize);
    lOBJ := TConverterObjectBytes.BytesToObject(lData);
    Result := TCashObjectItem.Create;
    Result.ID := lObjectId;
    Result.ItemObject := lOBJ;
    Result.UseLast  := UnixToDateTime(lLastDatetime);
    Result.UseCount := lLastCount;
  end;
end;

function TCashFile.FindObjectWrite(ACashObjectItem: TCashObjectItem): Int64;
var
  lPos: Int64;
  lCount: Integer;
  lObjectId: Integer;
  lIsDelete: Byte;
  lLastDatetime: Int64;
  lLastCount: Integer;
  lSize: Integer;
  lData: TBytes;
  lOBJ: TObject;
begin
  lPos := FStream.Size;
  FStream.Position := lPos;
  lObjectId     := ACashObjectItem.ID;
  lIsDelete     := 0;
  lLastDatetime := DateTimeToUnix(ACashObjectItem.UseLast);
  lLastCount    := ACashObjectItem.UseCount;
  lCount := FStream.Write(lObjectId, SizeOf(lObjectId));
  lCount := FStream.Write(lIsDelete, SizeOf(lIsDelete));
  lCount := FStream.Write(lLastDatetime, SizeOf(lLastDatetime));
  lCount := FStream.Write(lLastCount, SizeOf(lLastCount));
  lOBJ := ACashObjectItem.ItemObject;
  lData := TConverterObjectBytes.ObjectToBytes(lOBJ);
  lSize := Length(lData);
  lCount := FStream.Write(lSize, SizeOf(lSize));
  lCount := FStream.Write(lData, lSize);
  Result := FStream.Position;
  Count := Count + 1;
end;

function TCashFile.FindObjectPos(AObjectID: Integer): Int64;
begin
  Result := -1;
  Assert(FStream <> nil, 'No cash file');
  FStream.Position := 0;
  Result := StreamFindObjectPos(AObjectID);
end;

procedure TCashFile.Flush;
var
  lTMPFile: TFileName;
  lTMPFileStream: TFileStream;
  lCount: Integer;
  lByte: Integer;
  lObjectId: Integer;
  lIsDelete: Byte;
  lLastDatetime: Int64;
  lLastCount: Integer;
  lSize: Integer;
  lData: TBytes;
  lPosition: Int64;
  lPacketSize: Integer;
begin
  lTMPFile := TPath.GetTempFileName;
  lTMPFileStream := TFileStream.Create(lTMPFile, fmCreate);
  try
    lCount := 0;
    FStream.Position := 0;
    while FStream.Position < FStream.Size do begin
      lPosition := FStream.Position;
      lByte := FStream.Read(lObjectId, SizeOf(lObjectId));
      lByte := FStream.Read(lIsDelete, SizeOf(lIsDelete));
      lByte := FStream.Read(lLastDatetime, SizeOf(lLastDatetime));
      lByte := FStream.Read(lLastCount, SizeOf(lLastCount));
      lByte := FStream.Read(lSize, SizeOf(lSize));
      FStream.Position := FStream.Position + lSize;
      lPacketSize := FStream.Position - lPosition;
      if lIsDelete = 0 then begin
        FStream.Position := lPosition;
        lTMPFileStream.CopyFrom(FStream, lPacketSize);
        Inc(lCount);
      end;
    end;
  finally
    lTMPFileStream.Free;
    if lCount > 0 then begin
      StreamFree;
      tfile.Copy(lTMPFile, FileName, True);
      FStream := TFileStream.Create(FFileName, fmOpenReadWrite + fmShareDenyNone);
    end;
  end;
end;

function TCashFile.GetObjectItem(AObjectID: Integer): TCashObjectItem;
begin
  Result := FindObjectRead(AObjectID);
end;

function TCashFile.HEXStreamReadConverted(AStream: TStream;
  const ADataConvertedSize: Integer): TBytes;
var
  lHEXBytes: TBytes;
  lBytes: TBytes;
begin
  SetLength(lBytes, ADataConvertedSize);
  SetLength(lHEXBytes, ADataConvertedSize * 2);
  AStream.Read(lHEXBytes, Length(lHEXBytes));
  HexToBin(lHEXBytes, 0, lBytes, 0, Length(lBytes));
  Result := lBytes;
end;

function TCashFile.HEXStreamWriteConverted(AStream: TStream;
  const ADataConverted: TBytes): Integer;
var
  lHEXBytes: TBytes;
  lBytes: TBytes;
begin
  SetLength(lHEXBytes, Length(ADataConverted) * 2);
  BinToHex(lHEXBytes, 0, lBytes, 0, Length(lBytes));
  AStream.Write(lHEXBytes, Length(lHEXBytes));
  Result := Length(lHEXBytes);
end;

function TCashFile.Next(AIndex: Integer = 1): TCashObjectItem;
var
  lObjectId: Integer;
  lIsDelete: Byte;
  lCount: Integer;
  lLastDatetime: Int64;
  lLastCount: Integer;
  lSize: Integer;
  lData: TBytes;
  lItem: TCashObjectItem;
  lPos: Int64;
begin
  if AIndex = 0 then
    FStream.Position := 0;
  Result := nil;
  lPos := StreamFindObjectPos(-1);
  if (lPos >= 0) and (lPos < FStream.Size) then begin
    FStream.Position := lPos;
    lCount := FStream.Read(lObjectId, SizeOf(lObjectId));
    lCount := FStream.Read(lIsDelete, SizeOf(lIsDelete));
    lCount := FStream.Read(lLastDatetime, SizeOf(lLastDatetime));
    lCount := FStream.Read(lLastCount, SizeOf(lLastCount));
    lCount := FStream.Read(lSize, SizeOf(lSize));

    lItem := TCashObjectItem.Create;
    lItem.ID       := lObjectId;
    lItem.UseLast  := UnixToDateTime(lLastDatetime);
    lItem.UseCount := lLastCount;
    if AIndex <> -1 then begin
      // ���� ����� �������� ������ ��������� ������, �� �� ����� ��������� ��� ������������ ������
      SetLength(lData, lSize);
      lCount := FStream.Read(lData, lSize);
      lItem.ItemObject := TConverterObjectBytes.BytesToObject(lData);
    end else begin
      // ���������� ������ �������
      FStream.Position := FStream.Position + lSize;
    end;
    Result := lItem;
  end;
end;

procedure TCashFile.SetFileName(const Value: TFileName);
begin
  FFileName := Value;
  StreamFree;
  if not FileExists(FFileName) then begin
    FStream := TFileStream.Create(FFileName, fmCreate);
    FStream.Free;
  end;
  FStream := TFileStream.Create(FFileName, fmOpenReadWrite + fmShareDenyNone);
  Clear;
end;

function TCashFile.SetObjectItem(
  ACashObjectItem: TCashObjectItem): TCashObjectItem;
begin
  FindObjectWrite(ACashObjectItem);
  Result := ACashObjectItem;
end;

function TCashFile.SetObjectItem(AObjectItem: TObject;
  AObjectID: Integer): TCashObjectItem;
var
  lItem: TCashObjectItem;
begin
  lItem := TCashObjectItem.Create;
  lItem.ID := AObjectID;
  lItem.ItemObject := AObjectItem;
  Result := SetObjectItem(lItem);
end;

function TCashFile.StreamFindObjectPos(AObjectID: Integer): Int64;
var
  lObjectId: Integer;
  lIsDelete: Byte;
  lCount: Integer;
  lLastDatetime: Int64;
  lLastCount: Integer;
  lSize: Integer;
  lResult: Int64;
begin
  Result := -1;
  Assert(FStream <> nil, 'No cash file');
  while (FStream.Position < FStream.Size) do
  begin
    FCurrentPos := FStream.Position;
    lResult := FStream.Position;
    lCount := FStream.Read(lObjectId, SizeOf(lObjectId));
    lCount := FStream.Read(lIsDelete, SizeOf(lIsDelete));
    lCount := FStream.Read(lLastDatetime, SizeOf(lLastDatetime));
    lCount := FStream.Read(lLastCount, SizeOf(lLastCount));
    lCount := FStream.Read(lSize, SizeOf(lSize));
    if AObjectID = -1 then begin
      if lIsDelete = 0 then
        Break;
    end else begin
      if (AObjectID = lObjectId) and (lIsDelete = 0) then
        Break;
    end;
    FStream.Position := FStream.Position + lSize;
  end;
  if FStream.Position < FStream.Size then
    Result := lResult;
end;

procedure TCashFile.StreamFree;
begin
  if Assigned(FStream) then begin
    FStream.Free;
    FStream := nil;
  end;
end;

{ TConverterObjectBytes }

class function TConverterObjectBytes.BytesToObject(ABytes: TBytes): TObject;
var
  lClassId: string;
  lOffset: Integer;
  lrttiCTX: TRttiContext;
  lObjType: TRttiType;
  lrttiFields: TArray<TRttiField>;
  i: Integer;
  lField: TRttiField;
  lValue: TValue;
  lOBJ: TObject;
begin
  lOffset := BytesToStr(ABytes, lClassId, 0);

  lOBJ := NewInstance(lClassId);
  lrttiCTX := TRttiContext.Create;
  lObjType := lrttiCTX.GetType(lOBJ.ClassInfo);
  lrttiFields := lObjType.GetFields;
  for i := Low(lrttiFields) to High(lrttiFields) do begin
    lField := lrttiFields[i];
    lValue := lField.GetValue(lOBJ);
    lOffset := SimpleTypeRevert(lValue, ABytes, lOffset);
    lField.SetValue(lOBJ, lValue);
  end;

  Result := lOBJ;
end;

class function TConverterObjectBytes.BytesToStr(ASource: TBytes; var AStr: string;
  AOffset: Integer): Integer;
var
  lStr: TBytes;
  lSize: Integer;
begin
  CopyMemory(@lSize, @ASource[AOffset], SizeOf(lSize));
  SetLength(lStr, lSize);
  CopyMemory(@lStr[0], @ASource[AOffset + SizeOf(lSize)], Length(lStr));
  AStr := StringOf(lStr);
  Result := AOffset + lSize + SizeOf(lSize);
end;

class function TConverterObjectBytes.GetClassId(ASource: TObject): string;
begin
  Result := ASource.QualifiedClassName;
end;

class function TConverterObjectBytes.NewInstance(AClassId: string): TObject;
var
  ctx: TRttiContext;
  objType: TRttiType;
  lObj: TObject;
  lInst: TRttiInstanceType;
  lValue: TValue;
  lMethod: TRttiMethod;
  lConstrParametr: TArray<TRttiParameter>;
begin
  lObj := nil;
  ctx := TRttiContext.Create;
  objType := ctx.FindType(AClassId);
  if objType.IsInstance then begin
    lInst := objType.AsInstance;
    lMethod := lInst.GetMethod('Create');//.Invoke(lInst.MetaclassType, [nil]);
    if lMethod.IsConstructor then begin
      lConstrParametr := lMethod.GetParameters;
      if Length(lConstrParametr) = 0 then begin
        lValue := lMethod.Invoke(lInst.MetaclassType, []);
        if lValue.IsObject then
          lObj := lValue.AsObject;
      end;
    end;
  end;
  Result := lObj;
end;

class function TConverterObjectBytes.ObjectToBytes(ASource: TObject): TBytes;
var
  lClassId: string;
  lBytes: TBytes;
  lOffset: Integer;
  lrttiCTX: TRttiContext;
  lObjType: TRttiType;
  lrttiFields: TArray<TRttiField>;
  i: Integer;
  lField: TRttiField;
  lValue: TValue;
begin
  lClassId := GetClassId(ASource);
  lBytes := StrToBytes(lClassId);
  lOffset := Length(lBytes);

  lrttiCTX := TRttiContext.Create;
  lObjType := lrttiCTX.GetType(ASource.ClassInfo);
  lrttiFields := lObjType.GetFields;
  for i := Low(lrttiFields) to High(lrttiFields) do begin
    lField := lrttiFields[i];
    lValue := lField.GetValue(ASource);
    lOffset := SimpleTypeConvert(lValue, lBytes, lOffset);
  end;
  SetLength(lBytes, lOffset);
  Result := lBytes;
end;

class function TConverterObjectBytes.SimpleTypeConvert(AValue: TValue;
  var AData: TBytes; AOffset: Integer): Integer;
var
  lData: TBytes;
  lOffset: Integer;
  lInt: Integer;
  lDbl: Extended;
  lInt64: Int64;
  lStr: TBytes;
  lSize: Integer;
begin
  Result := -1;
  lOffset := 0;
  if AOffset >= 0 then
    lOffset := AOffset;

  case AValue.Kind of
    tkInteger: begin
      lInt := AValue.AsInteger;
      SetLength(lData, SizeOf(lInt));
      CopyMemory(@lData[0], @lInt, SizeOf(lInt));
    end;
    tkFloat: begin
      lDbl := AValue.AsExtended;
      SetLength(lData, SizeOf(lDbl));
      CopyMemory(@lData[0], @lDbl, SizeOf(lDbl));
    end;
    tkInt64: begin
      lInt64 := AValue.AsInt64;
      SetLength(lData, SizeOf(lInt64));
      CopyMemory(@lData[0], @lInt64, SizeOf(lInt64));
    end;
    tkString, tkChar, tkWChar, tkLString, tkWString, tkUString{,
    tkAnsiChar, tkWideChar, tkUnicodeString, tkAnsiString, tkWideString,
    tkShortString}: begin
      lData := StrToBytes(AValue.AsString);
    end;
    tkEnumeration: begin
      lInt64 := AValue.AsOrdinal;
      SetLength(lData, SizeOf(lInt64));
      CopyMemory(@lData[0], @lInt64, SizeOf(lInt64));
    end;
    tkSet: begin

    end;

  end;
//  AData := lData;
  if Length(lData) > 0 then begin
    if Length(lData) > (Length(AData) - AOffset) then
      SetLength(AData, Length(AData) + cnstBuffSize);

    CopyMemory(@AData[AOffset], @lData[0], Length(lData));
  end;
  Result := lOffset + Length(lData);
end;

class function TConverterObjectBytes.SimpleTypeRevert(var AValue: TValue;
  const AData: TBytes; AOffset: Integer): Integer;
var
  lOffset: Integer;
  lInt: Integer;
  lDbl: Extended;
  lInt64: Int64;
  lStr: TBytes;
  lSize: Integer;
  ss: string;
begin
  Result := -1;
  lOffset := 0;
  if AOffset >= 0 then
    lOffset := AOffset;

  case AValue.Kind of
    tkInteger: begin
      lSize := SizeOf(lInt);
      CopyMemory(@lInt, @AData[AOffset], SizeOf(lInt));
      AValue := lInt;
      lOffset := lOffset + SizeOf(lInt);
    end;
    tkFloat: begin
      SetLength(lStr, SizeOf(lDbl));
      CopyMemory(@lStr[0], @AData[AOffset], SizeOf(lDbl));
      CopyMemory(@lDbl, @AData[AOffset], SizeOf(lDbl));
      AValue := lDbl;
      lOffset := lOffset + SizeOf(lDbl);
    end;
    tkInt64: begin
      CopyMemory(@lInt64, @AData[AOffset], SizeOf(lInt64));
      AValue := lInt64;
      lOffset := lOffset + SizeOf(lInt64);
    end;
    tkString, tkChar, tkWChar, tkLString, tkWString, tkUString{,
    tkAnsiChar, tkWideChar, tkUnicodeString, tkAnsiString, tkWideString,
    tkShortString}: begin
      lOffset := BytesToStr(AData, ss, lOffset);
      AValue := ss;
    end;
    tkEnumeration: begin
      CopyMemory(@lInt64, @AData[AOffset], SizeOf(lInt64));
      AValue := TValue.FromOrdinal(AValue.TypeInfo, lInt64);
      lOffset := lOffset + SizeOf(lInt64);
    end;
    tkSet: begin

    end;

  end;

  Result := lOffset;
end;

class function TConverterObjectBytes.StrToBytes(ASource: string): TBytes;
var
  lStr: TBytes;
  lSize: Integer;
  lData: TBytes;
begin
  lStr := BytesOf(ASource);
  lSize := Length(lStr);
  SetLength(lData, lSize + SizeOf(lSize));
  CopyMemory(@lData[0], @lSize, SizeOf(lSize));
  CopyMemory(@lData[SizeOf(lSize)], @lStr[0], Length(lStr));
  Result := lData;
end;

end.
