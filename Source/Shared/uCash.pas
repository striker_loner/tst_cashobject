unit uCash;

interface

uses
  Classes, SysUtils, uCashObject, uCashMemory, uCashFile, DateUtils;

  {
  ���������� 2 ���� ����:
  1 - � ������ (uCashMemory)
  2 - � ��������� ����� (uCashFile)
  }
type
  TTransferCashObject = class(TCustomCash)
  private
    FCashFile: TCustomCash;
    FCashMemory: TCustomCash;
    procedure SetCashFile(const Value: TCustomCash);
    procedure SetCashMemory(const Value: TCustomCash);
  public
    property CashMemory: TCustomCash read FCashMemory write SetCashMemory;
    property CashFile: TCustomCash read FCashFile write SetCashFile;
  end;

  TCash = class(TCustomCash)
  private
    FCurrentObjectID: Integer;
    FTransfer: TTransferCashObject;
    procedure SetTransfer(const Value: TTransferCashObject);
  public
    constructor Create; override;
    destructor Destroy; override;

    property Transfer: TTransferCashObject read FTransfer write SetTransfer;

    function SetObjectItem(ACashObjectItem: TCashObjectItem): TCashObjectItem; overload; override;
    function SetObjectItem(AObjectItem: TObject; AObjectID: Integer = -1): TCashObjectItem; overload; override;
    function GetObjectItem(AObjectID: Integer): TCashObjectItem; override;
    procedure DeleteObjectItem(AObjectID: Integer); override;

    function Next(AIndex: Integer = 1): TCashObjectItem; override;

    procedure Clear; override;
    procedure Flush; override;
  end;

  TCashSingleton = class(TCash)
  strict private
    class var FInstance: TCashSingleton;
    constructor Create;
  public
    class function GetInstance: TCashSingleton;
  end;

  // ��� "��������" ����� ���� ����� ��������������� �������� ����� CashFile.Flush

  // ��������� �� ������� ���������
  TTransferCashObject_LastDate = class(TTransferCashObject)
  private
    FDateDeltaCashFile: Integer;
    FDateDeltaCashMemory: Integer;
    procedure SetDateDeltaCashFile(const Value: Integer);
    procedure SetDateDeltaCashMemory(const Value: Integer);
  public
    // ��������� �������
    property DateDeltaCashMemory: Integer read FDateDeltaCashMemory write SetDateDeltaCashMemory;
    property DateDeltaCashFile: Integer read FDateDeltaCashFile write SetDateDeltaCashFile;

    function SetObjectItem(ACashObjectItem: TCashObjectItem): TCashObjectItem; overload; override;
    function SetObjectItem(AObjectItem: TObject; AObjectID: Integer = -1): TCashObjectItem; overload; override;
    function GetObjectItem(AObjectID: Integer): TCashObjectItem; override;
    procedure DeleteObjectItem(AObjectID: Integer); override;

    function Next(AIndex: Integer = 1): TCashObjectItem; override;

    procedure Clear; override;
    procedure Flush; override;
  end;

  // ��������� �� ���������� ���������
  TTransferCashObject_LastCount = class(TTransferCashObject)
  private
    FCountCashFile: Integer;
    FCountCashMemory: Integer;
    procedure SetCountCashFile(const Value: Integer);
    procedure SetCountCashMemory(const Value: Integer);
  public
    // ��������� �������
    property CountCashMemory: Integer read FCountCashMemory write SetCountCashMemory;
    property CountCashFile: Integer read FCountCashFile write SetCountCashFile;

    function SetObjectItem(ACashObjectItem: TCashObjectItem): TCashObjectItem; overload; override;
    function SetObjectItem(AObjectItem: TObject; AObjectID: Integer = -1): TCashObjectItem; overload; override;
    function GetObjectItem(AObjectID: Integer): TCashObjectItem; override;
    procedure DeleteObjectItem(AObjectID: Integer); override;

    function Next(AIndex: Integer = 1): TCashObjectItem; override;

    procedure Clear; override;
    procedure Flush; override;
  end;

implementation

{ TCashSingleton }

constructor TCashSingleton.Create;
begin
  inherited Create;
end;

class function TCashSingleton.GetInstance: TCashSingleton;
begin
  if FInstance = nil then
    FInstance := Create;

  Result := FInstance;
end;

{ TCash }

procedure TCash.Clear;
begin
  if Assigned(FTransfer) then
    FTransfer.Clear;
end;

constructor TCash.Create;
begin
  inherited;
  FCurrentObjectID := 0;
  FTransfer := nil;
end;

procedure TCash.DeleteObjectItem(AObjectID: Integer);
begin
  if Assigned(Transfer) then
    FTransfer.DeleteObjectItem(AObjectID);
end;

destructor TCash.Destroy;
begin
  FTransfer.Free;
  inherited;
end;

procedure TCash.Flush;
begin
  if Assigned(FTransfer) then
    FTransfer.Flush;
end;

function TCash.GetObjectItem(AObjectID: Integer): TCashObjectItem;
begin
  if Assigned(Transfer) then
    Result := FTransfer.GetObjectItem(AObjectID);
end;

function TCash.Next(AIndex: Integer): TCashObjectItem;
begin
  if Assigned(FTransfer) then begin
    Result := FTransfer.Next(AIndex);
  end;
end;

function TCash.SetObjectItem(ACashObjectItem: TCashObjectItem): TCashObjectItem;
begin
  if Assigned(FTransfer) then begin
    Result := FTransfer.SetObjectItem(ACashObjectItem);
  end;
end;

function TCash.SetObjectItem(AObjectItem: TObject;
  AObjectID: Integer): TCashObjectItem;
var
  lObjectId: Integer;
begin
  lObjectId := AObjectID;
  if AObjectID < 0 then begin
    lObjectId := FCurrentObjectID;
    FCurrentObjectID := FCurrentObjectID + 1;
  end;
  Result := FTransfer.SetObjectItem(AObjectItem, lObjectId);
end;

procedure TCash.SetTransfer(const Value: TTransferCashObject);
begin
  FTransfer := Value;
end;

{ TTransferCashObject_LastDate }

procedure TTransferCashObject_LastDate.Clear;
begin
  FCashMemory.Clear;
  FCashFile.Clear;
end;

procedure TTransferCashObject_LastDate.DeleteObjectItem(AObjectID: Integer);
begin
  FCashMemory.DeleteObjectItem(AObjectID);
  FCashFile.DeleteObjectItem(AObjectID);
end;

procedure TTransferCashObject_LastDate.Flush;
var
  lItem: TCashObjectItem;
begin
  lItem := CashMemory.First;
  while Assigned(lItem) do begin
    if MilliSecondsBetween(Now, lItem.UseLast) > DateDeltaCashMemory then begin
      CashMemory.ExtractObjectItem(lItem.ID);
      CashFile.SetObjectItem(lItem);
      lItem.Free;
    end;
    lItem := CashMemory.Next();
  end;
  lItem := CashFile.First;
  while Assigned(lItem) do begin
    if MilliSecondsBetween(Now, lItem.UseLast) > DateDeltaCashFile then begin
      CashFile.DeleteObjectItem(lItem.ID);
    end;
    lItem.Free; // ��������� ������
    lItem := CashFile.Next(-1);
  end;
end;

function TTransferCashObject_LastDate.GetObjectItem(
  AObjectID: Integer): TCashObjectItem;
var
  lItem: TCashObjectItem;
begin
  lItem := FCashMemory.GetObjectItem(AObjectID);
  if not Assigned(lItem) then begin
    lItem := FCashFile.GetObjectItem(AObjectID);
    if Assigned(lItem) then begin
      FCashFile.DeleteObjectItem(AObjectID);
      FCashMemory.SetObjectItem(lItem);
    end;
  end;
  Result := lItem;
  if Assigned(lItem) then begin
    lItem.UseLast := Now;
    lItem.UseCount := lItem.UseCount + 1;
  end;
end;

function TTransferCashObject_LastDate.Next(AIndex: Integer): TCashObjectItem;
begin

end;

procedure TTransferCashObject_LastDate.SetDateDeltaCashFile(
  const Value: Integer);
begin
  FDateDeltaCashFile := Value;
end;

procedure TTransferCashObject_LastDate.SetDateDeltaCashMemory(
  const Value: Integer);
begin
  FDateDeltaCashMemory := Value;
end;

procedure TTransferCashObject.SetCashFile(
  const Value: TCustomCash);
begin
  FCashFile := Value;
end;

procedure TTransferCashObject.SetCashMemory(const Value: TCustomCash);
begin
  FCashMemory := Value;
end;

function TTransferCashObject_LastDate.SetObjectItem(
  ACashObjectItem: TCashObjectItem): TCashObjectItem;
begin
  Result := FCashMemory.SetObjectItem(ACashObjectItem);
end;

function TTransferCashObject_LastDate.SetObjectItem(AObjectItem: TObject;
  AObjectID: Integer): TCashObjectItem;
begin
  Result := FCashMemory.SetObjectItem(AObjectItem, AObjectID);
end;

{ TTransferCashObject_LastCount }

procedure TTransferCashObject_LastCount.Clear;
begin
  FCashMemory.Clear;
  FCashFile.Clear;
end;

procedure TTransferCashObject_LastCount.DeleteObjectItem(AObjectID: Integer);
begin
  FCashMemory.DeleteObjectItem(AObjectID);
  FCashFile.DeleteObjectItem(AObjectID);
end;

procedure TTransferCashObject_LastCount.Flush;
var
  lItem: TCashObjectItem;
begin
  lItem := CashMemory.First;
  while Assigned(lItem) do begin
    if lItem.UseCount < CountCashMemory then begin
      CashMemory.ExtractObjectItem(lItem.ID);
      CashFile.SetObjectItem(lItem);
      lItem.Free;
    end;
    lItem := CashMemory.Next();
  end;
  lItem := CashFile.First;
  while Assigned(lItem) do begin
    if lItem.UseCount < CountCashFile then begin
      CashFile.DeleteObjectItem(lItem.ID);
      lItem.Free;
    end;
    lItem := CashFile.Next();
  end;
end;

function TTransferCashObject_LastCount.GetObjectItem(
  AObjectID: Integer): TCashObjectItem;
var
  lItem: TCashObjectItem;
begin
  lItem := FCashMemory.GetObjectItem(AObjectID);
  if not Assigned(lItem) then begin
    lItem := FCashFile.GetObjectItem(AObjectID);
    if Assigned(lItem) then begin
      FCashFile.DeleteObjectItem(AObjectID);
      FCashMemory.SetObjectItem(lItem);
    end;
  end;
  Result := lItem;
  if Assigned(lItem) then begin
    lItem.UseLast := Now;
    lItem.UseCount := lItem.UseCount + 1;
  end;
end;

function TTransferCashObject_LastCount.Next(AIndex: Integer): TCashObjectItem;
begin

end;

procedure TTransferCashObject_LastCount.SetCountCashFile(const Value: Integer);
begin
  FCountCashFile := Value;
end;

procedure TTransferCashObject_LastCount.SetCountCashMemory(
  const Value: Integer);
begin
  FCountCashMemory := Value;
end;

function TTransferCashObject_LastCount.SetObjectItem(
  ACashObjectItem: TCashObjectItem): TCashObjectItem;
begin
  Result := FCashMemory.SetObjectItem(ACashObjectItem);
end;

function TTransferCashObject_LastCount.SetObjectItem(AObjectItem: TObject;
  AObjectID: Integer): TCashObjectItem;
begin
  Result := FCashMemory.SetObjectItem(AObjectItem, AObjectID);
end;

end.
