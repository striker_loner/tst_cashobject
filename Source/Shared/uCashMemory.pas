unit uCashMemory;

interface

uses
  Classes, SysUtils, uCashObject, Generics.Collections;

type
  TCashMemory = class(TCustomCash)
  private
    FCashList: TObjectList<TCashObjectItem>;
    FIndex: Integer;
    function IndexByObjectID(AObjectID: Integer): Integer;
  protected
    procedure SetCount(const Value: Integer); override;
    function GetCount: Integer; override;
  public
    constructor Create; override;
    destructor Destroy; override;

    function SetObjectItem(ACashObjectItem: TCashObjectItem): TCashObjectItem; overload; override;
    function SetObjectItem(AObjectItem: TObject; AObjectID: Integer = -1): TCashObjectItem; overload; override;
    function GetObjectItem(AObjectID: Integer): TCashObjectItem; override;
    function ExtractObjectItem(AObjectID: Integer): TCashObjectItem; override;
    procedure DeleteObjectItem(AObjectID: Integer); override;
    function Next(AIndex: Integer = 1): TCashObjectItem; override;
    procedure Clear; override;
    procedure Flush; override;
  end;

implementation

{ TCashMemory }

procedure TCashMemory.Clear;
begin

end;

constructor TCashMemory.Create;
begin
  FCashList := TObjectList<TCashObjectItem>.Create();
  inherited;
end;

procedure TCashMemory.DeleteObjectItem(AObjectID: Integer);
var
  idx: Integer;
begin
  idx := IndexByObjectID(AObjectID);
  if idx >= 0 then begin
    FCashList.Delete(idx);
  end;
end;

destructor TCashMemory.Destroy;
begin
  FCashList.Free;
  inherited;
end;

function TCashMemory.ExtractObjectItem(AObjectID: Integer): TCashObjectItem;
var
  idx: Integer;
  lItem: TCashObjectItem;
begin
  Result := nil;
  idx := IndexByObjectID(AObjectID);
  if idx >= 0 then begin
    lItem := FCashList.ExtractAt(idx);
    Result := lItem;
  end;
end;

procedure TCashMemory.Flush;
begin

end;

function TCashMemory.GetCount: Integer;
begin
  Result := FCashList.Count;
end;

function TCashMemory.GetObjectItem(AObjectID: Integer): TCashObjectItem;
var
  idx: Integer;
  lItem: TCashObjectItem;
begin
  Result := nil;
  idx := IndexByObjectID(AObjectID);
  if idx >= 0 then begin
    lItem := FCashList.Items[idx];
    Result := lItem;
  end;
end;

function TCashMemory.IndexByObjectID(AObjectID: Integer): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := 0 to FCashList.Count - 1 do begin
    if FCashList.Items[i].ID = AObjectID then begin
      Result := i;
      Break;
    end;
  end;
end;

function TCashMemory.Next(AIndex: Integer): TCashObjectItem;
begin
  Result := nil;
  if AIndex = 0 then
    FIndex := 0;

  if FIndex >= FCashList.Count then
    FIndex := -1;
  if FIndex >= 0 then
    Result := FCashList.Items[FIndex];
  FIndex := FIndex + 1;
end;

procedure TCashMemory.SetCount(const Value: Integer);
begin
  FCashList.Count := Value;
end;

function TCashMemory.SetObjectItem(
  ACashObjectItem: TCashObjectItem): TCashObjectItem;
var
  idx: Integer;
  lItem: TCashObjectItem;
begin
  Result := nil;
  idx := -1;
  if ACashObjectItem.ID > 0 then
    idx := IndexByObjectID(ACashObjectItem.ID);
  if idx < 0 then begin
    FCashList.Add(ACashObjectItem);
  end;
end;

function TCashMemory.SetObjectItem(AObjectItem: TObject; AObjectID: Integer): TCashObjectItem;
var
  idx: Integer;
  lItem: TCashObjectItem;
begin
  Result := nil;
  idx := -1;
  if AObjectID > 0 then
    idx := IndexByObjectID(AObjectID);
  if idx < 0 then begin
    lItem := TCashObjectItem.Create;
    lItem.ID := AObjectID;
    lItem.ItemObject := AObjectItem;
    FCashList.Add(lItem);
  end;
end;

end.
