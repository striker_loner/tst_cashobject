unit uSampleObject;

interface

uses
  Classes, SysUtils, uCashObject, uCash;

type
  TCashObject_First = class(TObject)
  private
    FInt: Integer;
    FBln: Boolean;
    procedure SetBln(const Value: Boolean);
    procedure SetInt(const Value: Integer);
  public
    property Int: Integer read FInt write SetInt;
    property Bln: Boolean read FBln write SetBln;
  end;

  TCashObject_Second = class(TObject)
  private
    FStr: string;
    FDbl: Double;
    procedure SetDbl(const Value: Double);
    procedure SetStr(const Value: string);
  public
    destructor Destroy; override;

    property Str: string read FStr write SetStr;
    property Dbl: Double read FDbl write SetDbl;
  end;

  TCashObject_FirstNext = class(TCashObject_First)
  private
    FStr: string;
    FI64: Int64;
    procedure SetI64(const Value: Int64);
    procedure SetStr(const Value: string);
  public
    property Str: string read FStr write SetStr;
    property I64: Int64 read FI64 write SetI64;
  end;

implementation


{ TCashObject_FirstNext }

procedure TCashObject_FirstNext.SetI64(const Value: Int64);
begin
  FI64 := Value;
end;

procedure TCashObject_FirstNext.SetStr(const Value: string);
begin
  FStr := Value;
end;

{ TCashObject_Second }

destructor TCashObject_Second.Destroy;
begin

  inherited;
end;

procedure TCashObject_Second.SetDbl(const Value: Double);
begin
  FDbl := Value;
end;

procedure TCashObject_Second.SetStr(const Value: string);
begin
  FStr := Value;
end;

{ TCashObject_First }

procedure TCashObject_First.SetBln(const Value: Boolean);
begin
  FBln := Value;
end;

procedure TCashObject_First.SetInt(const Value: Integer);
begin
  FInt := Value;
end;

initialization
end.
