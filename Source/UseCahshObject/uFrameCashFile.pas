unit uFrameCashFile;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, uSampleObject,
  uCashObject, uCashFile, Vcl.ExtCtrls;

type
  TfrCashFile = class(TFrame)
    btnAddTestObject: TButton;
    lbledtCashFileName: TLabeledEdit;
    btnCashFilename: TButton;
    dlgOpenCashFileName: TOpenDialog;
    btnCashINIT: TButton;
    procedure btnAddTestObjectClick(Sender: TObject);
    procedure btnCashFilenameClick(Sender: TObject);
    procedure btnCashINITClick(Sender: TObject);
  private
    FCashFile: TCashFile;
    { Private declarations }
    procedure CashFileInit;
    procedure CashFileFree;
  public
    destructor Destroy; override;
    { Public declarations }

  end;

implementation

{$R *.dfm}

procedure TfrCashFile.btnAddTestObjectClick(Sender: TObject);
var
  lSample: TCashObject_First;
  lCashItem: TCashObjectItem;
begin
  //
  lSample := TCashObject_First.Create;
  try
    lSample.Int := $87654321;
    lSample.Bln := True;

    lCashItem := FCashFile.SetObjectItem(lSample);
  finally
//    lSample.Free;
    lCashItem.Free;
  end;
end;

procedure TfrCashFile.btnCashFilenameClick(Sender: TObject);
begin
  if dlgOpenCashFileName.Execute then begin
    lbledtCashFileName.Text := dlgOpenCashFileName.FileName;
    CashFileFree;
  end;
end;

procedure TfrCashFile.btnCashINITClick(Sender: TObject);
begin
  CashFileInit;
end;

procedure TfrCashFile.CashFileFree;
begin
  FCashFile.Free;
  FCashFile := nil;
end;

procedure TfrCashFile.CashFileInit;
begin
  if not Assigned(FCashFile) then begin
    FCashFile := TCashFile.Create;
    FCashFile.FileName := lbledtCashFileName.Text;
  end;
end;

destructor TfrCashFile.Destroy;
begin
  CashFileFree;
  inherited;
end;

end.
