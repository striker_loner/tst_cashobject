unit uFrameCash;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls,
  uCash, uCashObject, uSampleObject, Vcl.ExtCtrls;

type
  TfrCash = class(TFrame)
    btnFlush: TButton;
    btnAddTestObject: TButton;
    lblCashMemoryCount: TLabel;
    lblCashFileCount: TLabel;
    lbledtObjectID: TLabeledEdit;
    btnGetCashObject: TButton;
    btnFileFlush: TButton;
    procedure btnFlushClick(Sender: TObject);
    procedure btnAddTestObjectClick(Sender: TObject);
    procedure btnGetCashObjectClick(Sender: TObject);
    procedure btnFileFlushClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

const
  cnstWORDS: array [0..99] of string = (
'tank', 'edge', 'annoying', 'drain', 'carve', 'makeshift', 'easy', 'spiffy'
, 'fertile', 'record', 'standing', 'screw', 'hollow', 'tawdry', 'inexpensive'
, 'egg', 'itch', 'slimy', 'afraid', 'wonder', 'shock', 'reply', 'shocking'
, 'unaccountable', 'grieving', 'loss', 'aspiring', 'program', 'sip', 'color'
, 'satisfy', 'bedroom', 'employ', 'distance', 'store', 'inquisitive', 'start'
, 'dust', 'silver', 'support', 'copper', 'quarter', 'onerous', 'lying', 'adventurous'
, 'ubiquitous', 'base', 'ocean', 'hall', 'damaged', 'pies', 'bolt', 'cobweb'
, 'hurry', 'airport', 'receive', 'noiseless', 'woebegone', 'wonderful', 'leather', 'delight'
, 'tip', 'abstracted', 'rural', 'rabid', 'selfish', 'shrill', 'glove', 'deliver'
, 'unpack', 'rub', 'chicken', 'wash', 'public', 'sweet', 'endurable', 'cats'
, 'fascinated', 'close', 'blush', 'coordinated', 'thoughtful', 'verdant', 'worthless'
, 'bit', 'defective', 'abhorrent', 'week', 'seed', 'cuddly', 'punishment', 'happy'
, 'birthday', 'continue', 'teaching', 'dangerous', 'successful', 'moan', 'legal'
, 'squirrel'
);

function RandomStr(AWordCount: Integer): string;
var
  i: Integer;
begin
  Result := cnstWORDS[Random(100)];
  for i := 0 to AWordCount - 1 do begin
    Result := Result + ' ' + cnstWORDS[Random(100)];
  end;
end;

{$R *.dfm}

procedure TfrCash.btnAddTestObjectClick(Sender: TObject);
var
  lCash: TCashSingleton;
  lSample: TObject;
begin
  lCash := TCashSingleton.GetInstance;
  case Random(3) of
    0: begin
      lSample := TCashObject_First.Create;
      (lSample as TCashObject_First).Int := Random(MaxInt);
      (lSample as TCashObject_First).Bln := Random(MaxInt) > (MaxInt div 2);
    end;
    1: begin
      lSample := TCashObject_Second.Create;
      (lSample as TCashObject_Second).Dbl := Random() * Random(MAXWORD);
      (lSample as TCashObject_Second).Str := RandomStr(Random(11));
    end;
    2: begin
      lSample := TCashObject_FirstNext.Create;
      (lSample as TCashObject_FirstNext).Int := Random(MaxInt);
      (lSample as TCashObject_FirstNext).Bln := Random(MaxInt) > (MaxInt div 2);
      (lSample as TCashObject_FirstNext).I64 := Random(MaxInt) shl 32 + Random(MaxInt);
      (lSample as TCashObject_FirstNext).Str := RandomStr(Random(11));
    end;
  end;
  lCash.SetObjectItem(lSample);
  lblCashMemoryCount.Caption := 'CashMemoryCount: ' + IntToStr(lCash.Transfer.CashMemory.Count);
  lblCashFileCount.Caption := 'CashFileCount: ' + IntToStr(lCash.Transfer.CashFile.Count);
end;

procedure TfrCash.btnFileFlushClick(Sender: TObject);
begin
  TCashSingleton.GetInstance.Transfer.CashFile.Flush;
end;

procedure TfrCash.btnFlushClick(Sender: TObject);
var
  lCash: TCashSingleton;
begin
  lCash := TCashSingleton.GetInstance;
  lCash.Flush;
  lblCashMemoryCount.Caption := 'CashMemoryCount: ' + IntToStr(lCash.Transfer.CashMemory.Count);
  lblCashFileCount.Caption := 'CashFileCount: ' + IntToStr(lCash.Transfer.CashFile.Count);
end;

procedure TfrCash.btnGetCashObjectClick(Sender: TObject);
var
  lObjectId: Integer;
  lCash: TCashSingleton;
begin
  lObjectId := StrToInt(lbledtObjectID.Text);
  lCash := TCashSingleton.GetInstance;
  lCash.GetObjectItem(lObjectId);
  lblCashMemoryCount.Caption := 'CashMemoryCount: ' + IntToStr(lCash.Transfer.CashMemory.Count);
  lblCashFileCount.Caption := 'CashFileCount: ' + IntToStr(lCash.Transfer.CashFile.Count);
end;

end.
