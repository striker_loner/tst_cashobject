unit uSettings;

interface

uses
  Classes, SysUtils, uINIPersist;

type
  TSettings = class
  private
    FCashFile: string;
    FTransfer: string;
    FTransfer_MemCount: Integer;
    FTransfer_MemTime: Integer;
    FTransfer_FileTime: Integer;
    procedure SetCashFile(const Value: string);
    procedure SetTransfer(const Value: string);
    procedure SetTransfer_FileTime(const Value: Integer);
    procedure SetTransfer_MemCount(const Value: Integer);
    procedure SetTransfer_MemTime(const Value: Integer);
  public
    [INIValue('OPTION', 'CashFile')]
    property CashFile: string read FCashFile write SetCashFile;
    [INIValue('OPTION', 'Transfer')]
    property Transfer: string read FTransfer write SetTransfer;
    [INIValue('TRANSFER', 'MemTime')]
    property Transfer_MemTime: Integer read FTransfer_MemTime write SetTransfer_MemTime;
    [INIValue('TRANSFER', 'FileTime')]
    property Transfer_FileTime: Integer read FTransfer_FileTime write SetTransfer_FileTime;
    [INIValue('TRANSFER', 'MemCount')]
    property Transfer_MemCount: Integer read FTransfer_MemCount write SetTransfer_MemCount;

    procedure LoadFrom(AFilename: TFileName);
  end;

  TSettingsSingleton = class(TSettings)
  strict private
    class var FInstance: TSettingsSingleton;
    constructor Create;
  public
    class function GetInstance: TSettingsSingleton;
  end;

implementation

{ TSettings }

procedure TSettings.LoadFrom(AFilename: TFileName);
begin
  if not FileExists(AFilename) then
    raise Exception.Create('Can''t load settings from: ' + AFilename);
  TIniPersist.Load(AFilename, Self);
end;

procedure TSettings.SetCashFile(const Value: string);
begin
  FCashFile := Value;
end;

procedure TSettings.SetTransfer(const Value: string);
begin
  FTransfer := Value;
end;

procedure TSettings.SetTransfer_FileTime(const Value: Integer);
begin
  FTransfer_FileTime := Value;
end;

procedure TSettings.SetTransfer_MemCount(const Value: Integer);
begin
  FTransfer_MemCount := Value;
end;

procedure TSettings.SetTransfer_MemTime(const Value: Integer);
begin
  FTransfer_MemTime := Value;
end;

{ TSettingsSingleton }

constructor TSettingsSingleton.Create;
begin
  inherited Create;
end;

class function TSettingsSingleton.GetInstance: TSettingsSingleton;
begin
  if FInstance = nil then
    FInstance := Create;

  Result := FInstance;
end;

end.
