object frCashFile: TfrCashFile
  Left = 0
  Top = 0
  Width = 460
  Height = 325
  TabOrder = 0
  DesignSize = (
    460
    325)
  object btnAddTestObject: TButton
    Left = 16
    Top = 104
    Width = 129
    Height = 25
    Caption = 'btnAddTestObject'
    TabOrder = 0
    OnClick = btnAddTestObjectClick
  end
  object lbledtCashFileName: TLabeledEdit
    Left = 16
    Top = 24
    Width = 409
    Height = 21
    Anchors = [akLeft, akTop, akRight]
    EditLabel.Width = 93
    EditLabel.Height = 13
    EditLabel.Caption = 'lbledtCashFileName'
    TabOrder = 1
    Text = 'TEST_CashFile.dat'
  end
  object btnCashFilename: TButton
    Left = 430
    Top = 22
    Width = 27
    Height = 25
    Anchors = [akTop, akRight]
    Caption = '...'
    TabOrder = 2
    OnClick = btnCashFilenameClick
  end
  object btnCashINIT: TButton
    Left = 16
    Top = 64
    Width = 75
    Height = 25
    Caption = 'btnCashINIT'
    TabOrder = 3
    OnClick = btnCashINITClick
  end
  object dlgOpenCashFileName: TOpenDialog
    Left = 128
    Top = 8
  end
end
