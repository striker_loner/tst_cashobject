program UseCashObject;

uses
  Vcl.Forms,
  uMain in 'uMain.pas' {frmMain},
  uCashObject in '..\Shared\uCashObject.pas',
  uCash in '..\Shared\uCash.pas',
  uSettings in 'uSettings.pas',
  uINIPersist in '..\..\ThirdParty\uINIPersist.pas',
  uSampleObject in 'uSampleObject.pas',
  uCashMemory in '..\Shared\uCashMemory.pas',
  uCashFile in '..\Shared\uCashFile.pas',
  uFrameCashFile in 'uFrameCashFile.pas' {frCashFile: TFrame},
  uFrameCash in 'uFrameCash.pas' {frCash: TFrame};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
