object frCash: TfrCash
  Left = 0
  Top = 0
  Width = 320
  Height = 240
  TabOrder = 0
  object lblCashMemoryCount: TLabel
    Left = 16
    Top = 8
    Width = 101
    Height = 13
    Caption = 'lblCashMemoryCount'
  end
  object lblCashFileCount: TLabel
    Left = 16
    Top = 24
    Width = 79
    Height = 13
    Caption = 'lblCashFileCount'
  end
  object btnFlush: TButton
    Left = 16
    Top = 40
    Width = 75
    Height = 25
    Caption = 'btnFlush'
    TabOrder = 0
    OnClick = btnFlushClick
  end
  object btnAddTestObject: TButton
    Left = 120
    Top = 40
    Width = 105
    Height = 25
    Caption = 'btnAddTestObject'
    TabOrder = 1
    OnClick = btnAddTestObjectClick
  end
  object lbledtObjectID: TLabeledEdit
    Left = 16
    Top = 96
    Width = 121
    Height = 21
    EditLabel.Width = 69
    EditLabel.Height = 13
    EditLabel.Caption = 'lbledtObjectID'
    TabOrder = 2
  end
  object btnGetCashObject: TButton
    Left = 152
    Top = 96
    Width = 113
    Height = 25
    Caption = 'btnGetCashObject'
    TabOrder = 3
    OnClick = btnGetCashObjectClick
  end
  object btnFileFlush: TButton
    Left = 20
    Top = 136
    Width = 75
    Height = 25
    Caption = 'btnFileFlush'
    TabOrder = 4
    OnClick = btnFileFlushClick
  end
end
