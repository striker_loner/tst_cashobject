unit uMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.ComCtrls, uFrameCashFile, uFrameCash,
  uCashObject, uCash, uSettings, uCashMemory, uCashFile;

type
  TfrmMain = class(TForm)
    pgcAll: TPageControl;
    tsCash: TTabSheet;
    tsCashMemory: TTabSheet;
    tsCashFile: TTabSheet;
    frCashFile: TfrCashFile;
    frCash: TfrCash;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;

implementation

{$R *.dfm}

procedure TfrmMain.FormCreate(Sender: TObject);
var
  lBasePath: TFileName;
  lSettings: TSettingsSingleton;
  lCash: TCashSingleton;
  lCashMemory: TCashMemory;
  lCashFile: TCashFile;
  lTransfer: TTransferCashObject;
begin
  {$IFDEF DEBUG}
  ReportMemoryLeaksOnShutdown := True;
  {$ENDIF}
  lBasePath := ExtractFilePath(Application.ExeName);
  lSettings := TSettingsSingleton.GetInstance;
  lSettings.LoadFrom(lBasePath + 'Settings.ini');

  Randomize;
  lCashMemory := TCashMemory.Create;
  lCashFile   := TCashFile.Create;
  lCashFile.FileName := lSettings.CashFile;
  // ������� ������ ����������
  if SameText(lSettings.Transfer, 'DATE') then begin
    lTransfer   := TTransferCashObject_LastDate.Create;
    (lTransfer as TTransferCashObject_LastDate).DateDeltaCashMemory := lSettings.Transfer_MemTime;
    (lTransfer as TTransferCashObject_LastDate).DateDeltaCashFile   := lSettings.Transfer_FileTime;
  end else if SameText(lSettings.Transfer, 'DATE') then begin
    lTransfer := TTransferCashObject_LastCount.Create;
    (lTransfer as TTransferCashObject_LastCount).CountCashMemory := lSettings.Transfer_MemCount;
    (lTransfer as TTransferCashObject_LastCount).CountCashFile   := -1;
  end;
  lTransfer.CashMemory := lCashMemory;
  lTransfer.CashFile := lCashFile;
  lCash := TCashSingleton.GetInstance;
  lCash.Transfer := lTransfer;
end;

procedure TfrmMain.FormDestroy(Sender: TObject);
var
  lCash: TCashSingleton;
begin
  lCash := TCashSingleton.GetInstance;
  lCash.Transfer.CashMemory.Free;
  lCash.Transfer.CashFile.Free;
  lCash.Transfer.Free;
end;

end.
