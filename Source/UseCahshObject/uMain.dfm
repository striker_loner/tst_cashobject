object frmMain: TfrmMain
  Left = 0
  Top = 0
  Caption = 'frmMain'
  ClientHeight = 299
  ClientWidth = 635
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object pgcAll: TPageControl
    Left = 0
    Top = 0
    Width = 635
    Height = 299
    ActivePage = tsCash
    Align = alClient
    TabOrder = 0
    object tsCash: TTabSheet
      Caption = 'tsCash'
      inline frCash: TfrCash
        Left = 0
        Top = 0
        Width = 627
        Height = 271
        Align = alClient
        TabOrder = 0
        ExplicitLeft = 152
        ExplicitTop = 31
      end
    end
    object tsCashMemory: TTabSheet
      Caption = 'tsCashMemory'
      ImageIndex = 1
    end
    object tsCashFile: TTabSheet
      Caption = 'tsCashFile'
      ImageIndex = 2
      inline frCashFile: TfrCashFile
        Left = 0
        Top = 0
        Width = 627
        Height = 271
        Align = alClient
        TabOrder = 0
        ExplicitLeft = 112
        ExplicitTop = -54
        inherited lbledtCashFileName: TLabeledEdit
          Width = 576
          EditLabel.ExplicitLeft = 0
          EditLabel.ExplicitTop = -16
        end
        inherited btnCashFilename: TButton
          Left = 597
        end
      end
    end
  end
end
